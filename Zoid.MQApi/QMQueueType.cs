﻿using IBM.WMQ;

namespace Zoid.MQApi
{
    public enum QMQueueType
    {
        Local = MQC.MQQT_LOCAL,
        Remote = MQC.MQQT_REMOTE,
        Model = MQC.MQQT_MODEL,
        Alias = MQC.MQQT_ALIAS,
        Unknown = -1
    }
}
