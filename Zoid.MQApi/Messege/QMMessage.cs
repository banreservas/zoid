﻿using System;

namespace Zoid.MQApi.Messege
{
    /// <summary>
    /// Type which abstract a message at QueueManager
    /// </summary>
    public class QMMessage
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">Value to be put at Queue</param>
        public QMMessage(string text)
        {
            Text = text;
            StartTime = DateTime.Now;
        }

        /// <summary>
        /// Value to be put at Queue
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Moment of creation of the message
        /// </summary>
        public DateTime StartTime { get; set; }


        public override string ToString()
        {
            return Text;
        }
    }
}
