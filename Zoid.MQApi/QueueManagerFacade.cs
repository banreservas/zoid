﻿using IBM.WMQ;
using log4net;
using Zoid.MQApi.Configuration;
using Zoid.MQApi.Exceptions;

namespace Zoid.MQApi
{
    public class QueueManagerFacade
    {
        private readonly MQQueueManager _mqQueueManager;
        private readonly ILog _logger = LogManager.GetLogger(typeof(QueueManagerFacade));
        /// <summary>
        /// Instance object which contains each connection value neeeded to create a clear connection with the QueueManager
        /// </summary>
        public QMConfiguration Configuration { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration">Instance object which contains each connection value neeeded to create a clear connection with the QueueManager</param>
        public QueueManagerFacade(QMConfiguration configuration)
        {
            Configuration = configuration;

            try
            {
                _mqQueueManager = MQQueueManagerFactory.CreateNewMQQueueManager(configuration);
                _logger.Debug("QMQueueManager instance was created successfully");
            }
            catch (QMApiException qmApiException)
            {
                _logger.Debug("error occur when was trying to get a MQQueManager instance", qmApiException);
                throw;
            }
        }



        
    }
}
