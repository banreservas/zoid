﻿using System;
using System.Collections;
using IBM.WMQ;
using Zoid.MQApi.Configuration;
using Zoid.MQApi.Exceptions;

namespace Zoid.MQApi
{
    sealed class MQQueueManagerFactory
    {
        public static MQQueueManager CreateNewMQQueueManager(QMConfiguration configuration)
        {
            MQQueueManager instance;
            try
            {
                instance = new MQQueueManager(configuration.QueueManagerName, GetHashtableProperties(configuration));
            }
            catch (MQException mqException)
            {
                throw ExceptionFactory.CreateFromException("Some error occurs when a new MQQueueManager instance was tried to be created", mqException);
            }
            catch (Exception exception)
            {                
                throw ExceptionFactory.CreateFromException("Some error occurs when a new MQQueueManager instance was tried to be created", exception);
            }
            
            return instance;
        }

        private static Hashtable GetHashtableProperties(QMConfiguration configuration)
        {
            Hashtable props = new Hashtable();
            props.Add(MQC.HOST_NAME_PROPERTY, configuration.Host);
            props.Add(MQC.CHANNEL_PROPERTY, configuration.ChannelName);
            props.Add(MQC.PORT_PROPERTY, configuration.Port);
            props.Add(MQC.TRANSPORT_PROPERTY, configuration.TransportType.ToString());
            //props.Add(MQC.CONNECTION_NAME_PROPERTY, configuration.ConnectionName);
            return props;
        }
    }
}
