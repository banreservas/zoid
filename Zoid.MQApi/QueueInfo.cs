﻿using System.Text;

namespace Zoid.MQApi
{
    /// <summary>
    /// This type hold al basic information of a specific Queue
    /// </summary>
    public class QueueInfo
    {
        /// <summary>
        /// Name of the Queue
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Type of Queue
        /// </summary>
        public QMQueueType Type { get; protected set; }

        /// <summary>
        /// This property give the current count of applications putting messages to this queue
        /// </summary>
        public int OpenInputCount { get; protected set; }
        /// <summary>
        /// This property give the current count of applications getting messages from this queue
        /// </summary>
        public int OpenOutputCount { get; protected set; }

        /// <summary>
        /// Give the count of messages holded on this queue
        /// </summary>
        public int CurrentDepth { get; protected set; }

        /// <summary>
        /// This property give the max of message allowed in the queue
        /// </summary>
        public int MaxDepth { get; protected set; }

        /// <summary>
        /// True if allows put message on queue and false if not
        /// </summary>
        public bool IsAllowedPutMessages { get; protected set; }

        /// <summary>
        /// True if allows get message on queue and false if not
        /// </summary>
        public bool IsAllowedGetMessages { get; protected set; }

        /// <summary>
        /// Maximum length of message 
        /// </summary>
        public int MaxMessageLength { get; protected set; }


        public override string ToString()
        {
            return new StringBuilder()
                .Append("QueueInfo { ")
                .AppendFormat("Name: {0}, ", Name)
                .AppendFormat("Type: {0}, ", Type)
                .AppendFormat("OpenInputCount: {0}, ", OpenInputCount)
                .AppendFormat("OpenOutputCount: {0}, ", OpenOutputCount)
                .AppendFormat("CurrentDepth: {0}, ", CurrentDepth)
                .AppendFormat("MaxDepth: {0}, ", MaxDepth)
                .AppendFormat("IsAllowedPutMessages: {0}, ", IsAllowedPutMessages)
                .AppendFormat("IsAllowedGetMessages: {0}, ", IsAllowedGetMessages)
                .AppendFormat("MaxMessageLength: {0} ", MaxMessageLength)
                .Append("}")
                .ToString();

        }


        #region Builder Class
        
        public class Builder
        {
            private readonly QueueInfo _queueInfo = new QueueInfo();

            public Builder SetQueueName(string queueName)
            {
                _queueInfo.Name = queueName;
                return this;
            }

            public Builder SetType(QMQueueType type)
            {
                _queueInfo.Type = type;
                return this;
            }

            public Builder SetOpenInputCount(int openInputCount)
            {
                _queueInfo.OpenInputCount = openInputCount;
                return this;
            }

            public Builder SetOpenOutputCount(int openOutputCount)
            {
                _queueInfo.OpenOutputCount = openOutputCount;
                return this;
            }

            public Builder SetCurrentDepth(int currentDepth)
            {
                _queueInfo.CurrentDepth = currentDepth;
                return this;
            }

            public Builder SetMaxDepth(int maxDepth)
            {
                _queueInfo.MaxDepth = maxDepth;
                return this;
            }

            public Builder SetAllowedPutMessages(bool isAllowedPutMessages)
            {
                _queueInfo.IsAllowedPutMessages = isAllowedPutMessages;
                return this;
            }

            public Builder SetAllowedGetMessages(bool isAllowedGetMessages)
            {
                _queueInfo.IsAllowedGetMessages = isAllowedGetMessages;
                return this;
            }

            public Builder SetMaxMessageLength(int maxMessageLength)
            {
                _queueInfo.MaxMessageLength = maxMessageLength;
                return this;
            }

            public QueueInfo Build()
            {
                return _queueInfo;
            }
        }
        #endregion
    }
}