﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoid.MQApi.Configuration
{
    /// <summary>
    /// This class hold all properties required and optionals needed to get a clear connection with a QueueManager
    /// </summary>
    public class QMConfiguration
    {
        /// <summary>
        /// Default constructor 
        /// </summary>
        public QMConfiguration() : this(String.Empty, String.Empty, String.Empty)
        {
            
        }

        /// <summary>
        /// Constructor with the minimum required arguments to get a clear connection with a QueueManager
        /// </summary>
        /// <param name="host">IP or Hostname where QueueManager is hosted or running</param>
        /// <param name="queueManangerName">Name of the QueueManager to be connected</param>
        /// <param name="channelName">ChannelName configured at hosted QueueManager</param>
        public QMConfiguration(string host, string queueManangerName, string channelName) 
        {
            Host = host;
            QueueManagerName = queueManangerName;
            ChannelName = channelName;
            TransportType = QMTransportType.TCP;
            Port = 1414;
            ConnectionName = $"{Host}({Port})";
        }
        /// <summary>
        /// Constructor with all argument needed to get a clear connection with a QueueManager
        /// </summary>
        /// <param name="host">IP or Hostname where QueueManager is hosted or running</param>
        /// <param name="queueManagerName">Name of the QueueManager to be connected</param>
        /// <param name="channelName">ChannelName configured at hosted QueueManager</param>
        /// <param name="port">When QueueManagers are created by default use port 1414 </param>
        /// <param name="transportType">Transport Type of the connection which is defined TCP normaly</param>
        public QMConfiguration(string host, string queueManagerName, string channelName, int port, QMTransportType transportType) : this(host, queueManagerName,channelName)
        {
            Port = port;
            TransportType = transportType;
            ConnectionName = $"{Host}({Port})";
        }
        /// <summary>
        /// IP or Hostname where QueueManager is hosted or running
        /// </summary>
        public String Host { get; set; }

        /// <summary>
        /// Name of the QueueManager to be connected
        /// </summary>
        public String QueueManagerName { get; set; }
        
        /// <summary>
        /// ChannelName configured at hosted QueueManager
        /// </summary>
        public String ChannelName { get; set; }

        /// <summary>
        /// When QueueManagers are created by default use port 1414
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Transport Type of the connection which is defined TCP normaly
        /// </summary>
        public QMTransportType TransportType{ get; set; }

        /// <summary>
        /// Connection Name
        /// </summary>
        public String ConnectionName { get; set; }
    }
}
