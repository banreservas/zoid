﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoid.MQApi.Configuration
{
    /// <summary>
    /// Types for Transport ways allowed
    /// </summary>
    public enum QMTransportType
    {
        TCP
    }

    /// <summary>
    /// This class hold special methods for QMTransportType Type
    /// </summary>
    public static class QMTransportTypeExtension
    {
        /// <summary>
        /// Cast to String Type each QMTransportType Enum Value
        /// </summary>
        /// <param name="qmTransportType">Enum value to be casted to String Type</param>
        /// <returns></returns>
        public static String ToString(this QMTransportType qmTransportType)
        {
            return Enum.GetName(typeof(QMTransportType), qmTransportType);
        }
    }
}
