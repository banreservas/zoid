﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoid.MQApi.Exceptions
{
    public class QMApiException : Exception
    {
        public QMApiException() : base()
        {
            
        }

        public QMApiException(String message) : base(message)
        {
                
        }

        public QMApiException(String message, Exception innerException) : base(message, innerException)
        {
            
        }
        
    }
}
