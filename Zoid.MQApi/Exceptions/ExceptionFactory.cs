﻿using System;
using IBM.WMQ;

namespace Zoid.MQApi.Exceptions
{
    public class ExceptionFactory
    {
        public static QMApiException CreateFromException(string message, Exception exception)
        {
            return new QMApiException(message, exception);
        }

        public static QMApiException CreateFromMQException(string message, MQException mqException)
        {
            return CreateFromException(string.Format("{3}\n\nMessage: {0} --------- Reason: {1} ------------- Reason Code: {2}",
                                                            mqException.Message,
                                                            mqException.Reason,
                                                            mqException.ReasonCode,
                                                            message),
                                                        mqException);
        }
    }
}
